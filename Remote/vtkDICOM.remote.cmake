#
# Dicom Classes
#

vtk_fetch_module(vtkDICOM
  "Dicom classes and utilities"
  GIT_REPOSITORY https://github.com/dgobbi/vtk-dicom
  # vtk-dicom tag v0.7.4 (Jan 29, 2016)
  GIT_TAG 4a7aff1210b8f58b80c8da66d8b761ee94594f4e
  )
